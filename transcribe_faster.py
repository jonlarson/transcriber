from time import sleep
from faster_whisper import WhisperModel
import logging
import os
import datetime
import re

logging.basicConfig()
logging.getLogger("faster_whisper").setLevel(logging.DEBUG)

BACKLOG_DIR = "backlog"
FINISHED_DIR = "finished"
TEXT_FILE_POSTFIX = ".txt"

def get_file() -> str | None:
    print("Getting files...")
    files = os.listdir(BACKLOG_DIR)
    # Do not use files that start with '.'
    files = list(filter(lambda x: not re.match(r'^\.', x), files))
    print(files)
    if files:
        print(f"Found files {files}")
        return files[0]
    else:
        print("No files found.")
        return None

def move_finished_file(file_path: str):
    print(f"Moving finished file for job '{file_path}'")
    # Move original file
    os.rename(
        "./" + BACKLOG_DIR + "/" + file_path,
        "./" + FINISHED_DIR + "/" + file_path
    )
    # Move the transcription
    os.rename(
        "./" + BACKLOG_DIR + "/" + file_path + TEXT_FILE_POSTFIX,
        "./" + FINISHED_DIR + "/" + file_path + TEXT_FILE_POSTFIX
    )

def run_transcribe(file_path: str):
    print(f"Starting transcription for '{file_path}'")
    # Customize 
    model_size = "large-v2"

    # Run on GPU with FP16
    # model = WhisperModel(model_size, device="cuda", compute_type="float16")
    # or run on GPU with INT8
    # model = WhisperModel(model_size, device="cuda", compute_type="int8_float16")
    # or run on CPU with INT8
    model = WhisperModel(model_size, device="cpu", compute_type="int8")

    segments, info = model.transcribe(file_path, beam_size=5)

    print("Detected language '%s' with probability %f" % (info.language, info.language_probability))

    with open(file_path + TEXT_FILE_POSTFIX, "w") as f:
        for segment in segments:
            start = datetime.timedelta(seconds=segment.start)
            end = datetime.timedelta(seconds=segment.end)
            s = f"[{str(start).split('.')[0]} -> {str(end).split('.')[0]}] {segment.text}"
            print(s)
            f.write(s + "\n")

if __name__ == "__main__":
    files_present = False
    print("Starting transcriber...")
    while True:
        file_path = get_file()
        if file_path:
            files_present = True
            run_transcribe("./" + BACKLOG_DIR + "/" + file_path)
            move_finished_file(file_path)
        else:
            files_present = False
        if not files_present:
            print("Waiting 30 seconds for more files")
            sleep(30)
