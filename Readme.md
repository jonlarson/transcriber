# Whisper Transcriber

This is a very basic script that will keep checking a folder for audio files, transcribe those that
are there, then move them into a "finished" folder with the transcription. No special scheduling or
concurrency, just that basic support for now.

The goal was an MVP that is portable - running on Mac, PC, Linux, etc. - and relatively simple to use.

## Easy Usage

1. Install Docker
1. Open this project in VS Code
1. Allow VS Code to open the dev container in `.devcontainer`
1. Once the devcontainer builds and installs dependencies, open a terminal
1. Place audio files in the `backlog` folder that you want to transcribe
1. In the terminal, start the script: `python transcribe_faster.py`
1. You will see it loop through the files, moving them and transcriptions into the `finished` folder

I am using the `large-v2` dataset, which on my M1 Max is taking about 
1/2 of realtime to transcode in the devcontainer with 20 GB of RAM.

## Related Projects

- OpenAI Whisper: https://github.com/openai/whisper
- Faster Whisper: https://github.com/guillaumekln/faster-whisper
- Whisper Webservice: https://github.com/ahmetoner/whisper-asr-webservice/
